# **Plugin for print label** #
External plugin for Tryton Client, create a label for print.

*TODO: more detailed explanation*

## **Edit file.glabels** ##
install program glabels

```
sudo apt -y install glabels
```

## **Install** ##
edit version tryton in file tryton.cfg and execute

```
sudo python3 install.py
```

or 

locate folder plugins tryton client
example: /usr/local/lib/python3.9/dist-packages/tryton/plugins

copy folder Glabels into module folder plugins tryton client

```
sudo cp -r Glabels/ /usr/local/lib/python3.9/dist-packages/tryton/plugins
```