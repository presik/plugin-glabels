#!/usr/bin/env python
#
# Script that install plug-ins in Tryton Client
import os
import shutil
import site
from configparser import ConfigParser

parser = ConfigParser()
parser.read('tryton.cfg')

version_ = parser.get('tryton', 'version')
for s in site.getsitepackages():
    if 'python3/dist-packages' in s:
        root_path = s
        break

# tryton_egg = 'tryton-' + version_ + '.egg-info'
srcdir = 'Glabels'
dstroot = os.path.join(root_path, 'tryton', 'plugins', srcdir)
source = os.path.dirname(os.path.realpath(__file__)) + '/Glabels'

print("dstroot :", dstroot)
if os.path.exists(dstroot):
    shutil.rmtree(dstroot)

print("source :", source)
shutil.copytree(source, dstroot)

print("################################################################")
print("Installing Widget Glabels for Tryton Client")
print("Version: : ", version_)
print( "################################################################")
