# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import os
import gettext
import tryton.rpc as rpc
import subprocess
import logging
import shutil
from tryton.common import RPCProgress

_ = gettext.gettext

HOME = os.environ['HOME']
SOURCE_FILE = os.path.dirname(os.path.realpath(__file__)) + '/etiquetas.csv'

PATH_GLABELS = os.path.join(HOME, 'Glabels', 'ETIQUETAS_TRYTON.glabels')
PATH_FILE = os.path.join(HOME, 'Glabels')


def exec_glabels(data):
    ctx = rpc.CONTEXT.copy()
    args = ('report', 'stock_shipment_barcode.shipment_tags_report',
        'execute', data.get('ids', []), data, ctx)
    res = RPCProgress('execute', args).run()

    if not res:
        return False
    (type, data, print_p, name) = res

    if not os.path.exists(PATH_FILE):
        os.makedirs(PATH_FILE)
        shutil.copy(SOURCE_FILE, PATH_FILE)

    with open(PATH_FILE + '/etiquetas.csv', 'w') as csv_file:
        data = data.decode('latin-1')
        csv_file.write(data)

    try:
        subprocess.Popen(['/usr/bin/glabels-3', PATH_GLABELS])
    except OSError:
        logging.exception('Error de impresion glabels')
        print('verify install library glabels')


def get_plugins(model):
    if model == 'stock.shipment.in':
        return [
            (_('Print GLabels'), exec_glabels),
        ]
    else:
        return []
